# nephioHelm

## Description
sample description

## Usage

### Fetch the package
`kpt pkg get REPO_URI[.git]/PKG_PATH[@VERSION] nephioHelm`
Details: https://kpt.dev/reference/cli/pkg/get/

### View package content
`kpt pkg tree nephioHelm`
Details: https://kpt.dev/reference/cli/pkg/tree/

### Apply the package
```
kpt live init nephioHelm
kpt live apply nephioHelm --reconcile-timeout=2m --output=table
```
Details: https://kpt.dev/reference/cli/live/
